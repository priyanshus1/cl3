import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Search Google example.
 *
 * @author Rahul
 */
public class SeleDemo {
    static WebDriver driver;
    static Wait<WebDriver> wait;

    public static void main(String[] args) {
        driver = new FirefoxDriver();
        wait = new WebDriverWait(driver, 30);
        driver.get("http://localhost:8080/cal.jsp");

        boolean result;
        try {
            result = firstPageContainsQAANet();
        } catch(Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            //driver.close();
        }

        System.out.println("Test " + (result? "passed." : "failed."));
        if (!result) {
            System.exit(1);
        }
    }

    private static boolean firstPageContainsQAANet() throws InterruptedException {
        //type search query
    	Thread.sleep(1000);
    	driver.findElement(By.xpath("//input[@type='submit']")).click();
        // click search
        //driver.findElement(By.name("btnG")).click();

        // Wait for search to complete
        /*wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                System.out.println("Searching ...");
                return webDriver.findElement(By.id("resultStats")) != null;
            }
        });*/

        // Look for QAAutomation.net in the results
    	Thread.sleep(1000);
    	//driver.findElement(By.xpath("//input[contains(@name,'result')]")).sendKeys("Patni");
    	System.out.println(driver.findElement(By.xpath("//input[contains(@name,'result')]")).getAttribute("value"));
        return driver.findElement(By.xpath("//input[contains(@name,'result')]")).getAttribute("value").contains("8");
    }
}
