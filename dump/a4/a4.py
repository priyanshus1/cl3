import threading
from time import sleep
import random
from pymongo import MongoClient

class Philosopher(threading.Thread):
    running= False
    @staticmethod
    def accessData():
        print "    def accessData():"
        client=MongoClient("localhost", 27017)
        db=client.test.dining
        db.
        return 2

    def __init__(self, i, leftLock, rightLock):
        threading.Thread.__init__(self)
        print "def __init__(self):"
        self.id=i
        self.lLock=leftLock
        self.rLock=rightLock
        
    def run(self):
        print "def run(self):"
        while Philosopher.running:
            sleep(5)
            self.hungry()
        
    def hungry(self):
        print "def hungry(self):"
        
        while Philosopher.running:
            l1, l2=self.lLock, self.rLock
            l1.acquire(True)
            locked=l2.acquire(False)
            if locked:
                break
            l1, l2=l2, l1
            l1.release()
        
        else:
            return 
        
        self.eat()
        l1.release()
        l2.release()
              
    def eat(self):
        print "def eat(self):"
        print "philosopher", self.id, "eating"
        data=Philosopher.accessData()
        print data
        
if __name__=="__main__":
    l=[threading.Lock() for n in range(5)] 
    philosophers=[Philosopher(i, l[i], l[(i+1)%5]) for i in range(5)]
    Philosopher.running=True
    for p in philosophers:
        p.start()
        sleep(20)
    Philosopher.running=False