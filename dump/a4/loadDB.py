from pymongo import MongoClient
from datetime import datetime

def loadData():
    file=open("data.txt", "r")
    try:
        client=MongoClient("localhost", 27017)
        db=client.test.dining
        for line in file:
            line=line.strip()
            line=line.split(',',1)
            line=[int(ele) for ele in line]
            print line[0]
            try:
                db.insert_one({"p":line[0], "temp":line[1], "time":str(datetime.now())})
            except:
                print "error inserting"
            print line
    except:
        print "connection error"
loadData()