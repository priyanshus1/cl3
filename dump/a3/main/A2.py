from xml.dom import minidom
from __builtin__ import True
import unittest
class BinaryS():
    DataList=[]
    pindex=0
    def getData(self,filename):
        self.DataList=[]
        readData=minidom.parse(filename)
        readLine=readData.getElementsByTagName('element')
        for x in readLine:
            self.DataList.append(int(x.getAttribute('index')))
        
        return len(self.DataList)-1
    
    def sort(self,low,high):
        if(low<high):
            self.pindex=self.partition(low,high)
    
            self.sort(self.pindex+1, high)
            self.sort(low, self.pindex-1)
    
    def partition(self,low,high):
        pivot=self.DataList[high]
        pindex=low
        for i in range(low,high):
            if(self.DataList[i]<=pivot):
                self.DataList[i],self.DataList[pindex]=self.DataList[pindex],self.DataList[i]
#                 temp=self.DataList[i]
#                 self.DataList[i]=self.DataList[pindex]
#                 self.DataList[pindex]=temp
                pindex+=1
                
        temp=self.DataList[high]
        self.DataList[high]=self.DataList[pindex]
        self.DataList[pindex]=temp        
        return pindex
    
    def binarySearch(self,data,low,high):
       
        if(low<=high):
            mid=int((low+high)/2)                
            if data==self.DataList[mid]:
                print "Found at Index",mid
            if(data<self.DataList[mid]):
                self.binarySearch(data, low, mid-1)
            elif(data>self.DataList[mid]):
                self.binarySearch(data, mid+1, high)
            
            return True
        
class mytest(unittest.TestCase):
    
    def testCase1(self):
            shukla=BinaryS()
            a=shukla.getData("../shuklalist.xml")
            shukla.sort(0,11)
            self.assertEqual(shukla.binarySearch(3,0,a), True)
            print shukla.DataList
            
        
        
        
        
if __name__=='__main__':
    shukla=BinaryS()
    a=shukla.getData("../shuklalist.xml")
    shukla.sort(0,11)
    data=input("De re Dada")
    shukla.binarySearch(int(data),0,a)
    print shukla.DataList
    unittest.main()
    
    
    