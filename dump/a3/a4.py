'''
Created on 15-Apr-2016

@author: shukla
'''
import threading
import time,random
from pymongo import MongoClient

class Philosopher(threading.Thread):
    connection=MongoClient("localhost",27017)
    
    def readFromMongo(self):
        db=self.connection.diniP.diniP
        cursor=db.find({"num":self.index})
        
        print cursor[1]["hi"]
        
    
    def __init__(self,x,name,forkL,forkR):
        threading.Thread.__init__(self)
        self.xname=name
        self.index=x
        self.forkL=forkL
        self.forkR=forkR
        
        
    def run(self):
        while(True):
            time.sleep(random.uniform(3,13))
            print "is hungry"
            self.dine()
            
    def dine(self):
        fork1=self.forkL
        fork2=self.forkR
        while True:
        
            fork1.acquire(True)
            locked=fork2.acquire(False)
            if locked: break
            fork1.release()
            fork1,fork2=fork2,fork1
            
        else:
            return
        
        self.dining()
        fork2.release
        fork1.release
    def dining(self):
        self.readFromMongo()
        time.sleep(random.uniform(1,10))
        
        
if __name__=='__main__':
    forkl=threading.Lock()
    forkR=threading.Lock()
    phil=Philosopher(0,"phil",forkl,forkR)
    phil.start()
   
        
        
        
