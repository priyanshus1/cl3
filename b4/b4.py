#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import bottle
from google.appengine.ext import db
 
class item(db.Model):
	name = db.StringProperty(required=True)
	qut=db.IntegerProperty()

app = bottle.default_app()
@app.route('/')
def helloworld():

	output = bottle.template('templates/insert')
	return output

@app.post('/insert')
def insert():
	na = bottle.request.forms.get("name")
	q= bottle.request.forms.get("number")	
	it=item(name=na, qut=int(q))
	db.put(it)
	return bottle.template('templates/view')

@app.route('/retrieve')
def readDB():
	st=item.all().order("-qut")
	return "Hello"

@app.route('/delete')
def clearDB():
	st=item.all()
	db.delete(st)
	return


