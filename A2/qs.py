from xml.dom import minidom
import random, time, sys
from multiprocessing import Pool, Process, Pipe
from itertools import chain
import os


def unwrap(arg, **kwarg):
    return QuickSort.partitionWrap(*arg, **kwarg)

class QuickSort:
    dataList=[]
    def __init__(self):
        pass

    def getData(self):
        self.dataList=[]
        xmldoc = minidom.parse('data.xml')
        itemlist = xmldoc.getElementsByTagName('Item')
        for s in itemlist:
            x=int(s.getAttribute("index").encode("utf-8"))
            self.dataList.append(x)
        print self.dataList

    def swap(self,a,i,j):
        temp = a[i]
        a[i] = a[j]
        a[j] = temp

    def partition(self,a,lo,hi):
            pivot = a[lo]
            #print pivot
            left=lo+1
            right=hi
            done=False
            while not done:
                while left<=right and a[left]<=pivot:
                    left=left+1
                while a[right]>pivot and right >=left:
                    right=right-1
                if right<left:
                    done=True
                else:
                    self.swap(a, left, right)
            self.swap(a, lo, right)
            return right


    def partitionWrap(self,i_list):
      	print "Pid is " ,os.getpid()
        ind, lyst = i_list
        if len(lyst) <= 1:
            return [lyst]
        print "Partiiton Wrap", lyst
	print "PPid is " ,os.getppid()
        b = self.partition(lyst, 0, len(lyst)-1)

        print (ind, [lyst[:b], [lyst[b]], lyst[b+1:]])
        return (ind, [lyst[:b], [lyst[b]], lyst[b+1:]])


    def QuickSortParallel(self,n):
        numproc = 2**n
        #Basically, we're going to partition the list until it's all
        #singletons. We'll store each singleton in the master list.
        ml = list(self.dataList)
        pool = Pool(processes = numproc)

        results = [(0, self.dataList)]
        #the one initial argument to partitionWrap
        #print results
        while len(results) > 0:
            temp = pool.map(unwrap, zip([self]*len(results), results))
            #Each element of temp is a list of up to three lists.
            results = []
            for i, plist in temp:
                for ll in plist: #for each little list in the partition output
                    if len(ll) == 1:
                        ml[i] = ll[0]
                        i += 1
                    elif len(ll) > 1:
                        results.append((i, ll))
                        i += len(ll)


        return ml

conSort=QuickSort()
conSort.getData()
n=3
sortedList=[]
sortedList=conSort.QuickSortParallel(n)
print sortedList



#output



[shukla@warware A2]$ python qs.py
[2, 4, 6, 10, 20, 1, 3, 15]
Pid is  5459
Partiiton Wrap [2, 4, 6, 10, 20, 1, 3, 15]
PPid is  5457
(0, [[1], [2], [6, 10, 20, 4, 3, 15]])
Pid is  5461
Partiiton Wrap [6, 10, 20, 4, 3, 15]
PPid is  5457
(2, [[4, 3], [6], [20, 10, 15]])
Pid is  5462
Partiiton Wrap [4, 3]
PPid is  5457
(2, [[3], [4], []])
Pid is  5464
Partiiton Wrap [20, 10, 15]
PPid is  5457
(5, [[15, 10], [20], []])
Pid is  5465
Partiiton Wrap [15, 10]
PPid is  5457
(5, [[10], [15], []])
[1, 2, 3, 4, 6, 10, 15, 20]
