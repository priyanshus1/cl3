from flask import Flask, render_template, redirect, url_for, jsonify
from mongoengine import *

app = Flask(__name__, template_folder = "common", static_folder = "common")

app.config["MONGODB_SETTINGS"] = {
	#"db" : "heroku_qrm53nmh",
	#"uri" : "mongodb://user1:user1@ds033875.mlab.com:33875/heroku_qrm53nmh"
	"db" : "cl3",
	"host" : "localhost",
	"port" : 27017
	
}

#Root, runs first on running the app
@app.route('/')
def home_page():
	return render_template('home.html')

#Function to retrieve data from mongo db
@app.route('/retrieve', methods = ["GET", "POST"])
def getData():
	from models import Number
	#connect(db = "heroku_qrm53nmh", host = "mongodb://user1:user1@ds033875.mlab.com:33875/heroku_qrm53nmh")
	connect(db = "cl3", host = "localhost", port = 27017)
	num_obj = Number.objects()
	
	return jsonify({"number" : num_obj[0].number })
